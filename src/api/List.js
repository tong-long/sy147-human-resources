
import request from '@/utils/request'

// 获取部门列表数据
export function List() {
  return request({
    url: '/company/department'
  })
}

// 获取部门负责人列表数据
export function DepartmentList() {
  return request({
    url: '/sys/user/simple'
  })
}

// 新增部门请求
export function postList(data) {
  return request({
    method: 'POSt',
    url: '/company/department',
    data
  })
}

// 删除部门请求
export function deleteList(id) {
  return request({
    method: 'DELETE',
    url: `/company/department/${id}`
  })
}

// 获取部门详情数据
export function GeTList(id) {
  return request({
    url: `/company/department/${id}`
  })
}

// 更新( 修改 ) 部门
export function PUTList(data) {
  return request({
    method: 'PUT',
    url: `/company/department/${data.id}`,
    data
  })
}

