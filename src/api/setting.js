
import request from '@/utils/request'

// 获取角色列表数据
export function GetUser(params) {
  return request({
    method: 'get',
    url: '/sys/role',
    params
  })
}

// 删除角色列表数据
export function deleteUser(id) {
  return request({
    method: 'DELETE',
    url: '/sys/role/' + id

  })
}

// 新增角色
export function PostUser(data) {
  return request({
    method: 'POST',
    url: '/sys/role',
    data
  })
}

// 获取角色详情数据
export function GetAllUser(id) {
  return request({
    method: 'GET',
    url: `/sys/role/${id}`
  })
}

// 更修(修改)角色详情数据
export function PUTAllUser(data) {
  return request({
    method: 'PUT',
    url: `/sys/role/${data.id}`,
    data
  })
}

