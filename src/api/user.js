import request from '@/utils/request'

// 登录函数
/**
 * 多行注释 目的是为了描述下面的函数
 * @description: 登录函数
 * @param {*} data { mobile,password }
 * @return {*} promise
 */

// 1. 入参 2.逻辑 3. return值
export function login(data) {
  // return 是 request函数的调用接口 也就是说它return的是request函数的返回值[promise对象]
  // return promise
  // baseURL + url  http://ihrm.itheima.net/api/sys/login
  return request({
    url: '/sys/login',
    method: 'POST',
    data: data // key data axios封装好的专门用来接受参数的位置
  })
}

// 如果你想进行登录 那就可以 调用login函数 调用的方式如下
// login({ mobile, password }).then(res => console.log(res))

// 1.封装的好处 [如何读懂一个函数]
// 1. 语义化 代码即注释  看到函数就知道作用
// 2. 复用性 多个业务中进行复用的时候 哪里用哪里导入
// 3. 函数统一化  项目写起来更有效率

// 2. requestjs和api关系
// 一对多关系  一：requestjs  多：api下面的所有的接口请求
// 比如：我们在拦截器中统一注入了token 只要你是使用我导出的request进行的接口封装
// 就都会自动拥有token数据 不需要单独再写

/**
 *
 */
export function getUserInfo() {
  return request({
    url: '/sys/profile',
    method: 'post'
  })
}

/**
 *
 */
export function getUserDetailById(id) {
  return request({
    // 这里的 method 可以省略 因为默认的是 get 获取
    url: `/sys/user/${id}`
  })
}
