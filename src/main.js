// mainjs 应用入口  项目的初始化 全局通用的事情

import Vue from 'vue'
// 引入一个初始化样式文件 margin padding
import 'normalize.css/normalize.css'

// 引入elementUI
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

import '@/styles/index.scss' // global css

// 引入根组件
import App from './App'

// 引入vuex
import store from './store'

// 引入路由router
import router from './router'

// 引入字体图标
import '@/icons' // icon

Vue.use(ElementUI) // ? 把elementUI中的组件注册为全局可用

// 导入permissionjs 模块中如果有同步代码会立刻执行
import './permission.js'

Vue.config.productionTip = false

// vue实例化
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
  // 类比为template模板 把app根组件进行渲染挂载 app根组件一单渲染
  // 底层的所有的子孙组件也会一起渲染
})
