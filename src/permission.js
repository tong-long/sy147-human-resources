// 权限控制
// 1. 路由跳转权限控制
// 2. 菜单权限控制

import router from '@/router'
import store from '@/store'
import { getToken } from './utils/auth'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
const WHITELIST = ['/login', '/404']

router.beforeEach((to, from, next) => {
  // to: 到哪里去 目标路由对象
  // from: 从哪里来 来源路由对象
  // next: 放行函数[在分支中都应该有且只有一个next]
  NProgress.start() // 开启进度条
  const token = getToken()
  if (token) {
    // 有token
    if (to.path === '/login') {
      next('/')
    } else {
      next()
      // 用户数据的获取位置很值的思考，其一用户资料的获取依赖于token，其二在跳转首页之后需要立刻获取，其三用户资料里还包含我们将来要做的菜单权限数据还需要满足权限语义化，综上，最合适触发action函数发送请求的位置就是permisson.js模块中跳转到首页的地方
      // 最合适的位置 满足以上三个条件 调用action函数
      // 如果当前已经调用过了 vuex已经存在数据 就不需要重复调用

      // 如果不存在这个 ID 证明从来没没有调用这个方法 我们在调用方法获取数据
      if (!store.state.user.userInfo.userId) {
        store.dispatch('user/fetchGetUserInfo')
      }
    }
  } else {
    // 没有token 数组中是否找得到某项 数组方法 includes(是否包含这个路径)
    if (WHITELIST.includes(to.path)) {
      // 在白名单内
      next()
    } else {
      next('/login')
    }
  }
  NProgress.done() // 结束进度条
})
