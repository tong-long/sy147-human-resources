// 导入 Layout 组件
import Layout from '@/layout'
// 所有的动态路由表， 都放在这里
// Layout组件作用：提供一个外层的框架 左侧 右边 顶部
// 二级路由path置空：如果当前的二级路由path为空 那么它
// 访问的是一级路由的路径 但是二级也会跟着一起渲染

// 动态路由
export const asyncRoutes = [
  // 工资管理
  {
    path: '/salary',
    component: Layout,
    children: [{
      path: '', // 如果children path置空的话 当前这个路由会作为一级渲染的默认路由
      name: 'Salary',
      component: () => import('@/views/Salary/index'),
      meta: { title: '工资管理', icon: 'money' }
    }]
  },
  // 组织架构
  {
    path: '/department',
    component: Layout,
    children: [{
      path: '',
      name: 'department',
      component: () => import('@/views/Department/index'),
      meta: { title: '组织架构', icon: 'tree' }
    }]
  },
  // 权限点
  {
    path: '/permission',
    component: Layout,
    children: [{
      path: '',
      name: 'permission',
      component: () => import('@/views/Permission/index'),
      meta: { title: '权限点管理', icon: 'dashboard' }
    }]
  },
  // 员工管理
  {
    path: '/employee',
    component: Layout,
    children: [{
      path: '',
      name: 'employee',
      component: () => import('@/views/Employee/index'),
      meta: { title: '员工管理', icon: 'people' }
    }]
  },
  // 角色管理
  {
    path: '/setting',
    component: Layout,
    children: [{
      path: '',
      name: 'setting',
      component: () => import('@/views/Setting/index'),
      meta: { title: '角色管理', icon: 'setting' }
    }]
  }
]
