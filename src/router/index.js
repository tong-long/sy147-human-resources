import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

// 使用两个独立的数组 分别管理动态路由和静态路由
// 所有的动态路由 不同用户看到的数量不一致
import { asyncRoutes } from '@/router/asyncRouter'

// 静态路由 ， 每个用户都可以看到
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/Login/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/Dashboard/index'),
      meta: { title: '首页', icon: 'dashboard' }
    }]
  }

]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  mode: 'history', // 路由模式 ， 默认是 hash 就是地址栏不带 #
  scrollBehavior: () => ({ y: 0 }), // 路由的滚动行为
  routes: [...constantRoutes, ...asyncRoutes]
})

const router = createRouter()

// reset 路由方法
export function resetRouter() {
  // 得到一个全新的router实例对象
  const newRouter = createRouter()
  // 使用新的路由记录覆盖掉老的路由记录
  // 包含了所有的路由记录 path - component
  router.matcher = newRouter.matcher // reset router
}

export default router
