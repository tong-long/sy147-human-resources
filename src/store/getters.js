// 全局getters
// 1. state的计算属性
// 2. 快捷访问 使用getters精简对于模块中响应式数据的获取 为了业务文件访问数据方便（拓展）

// user -> state -> userInfo -> name:柴柴老师
// this.$store.state.user.userInfo.name  比较长
// getters优化: name: state => state.user.userInfo.name
// this.$store.getters['name']

const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  userName: state => state.user.userInfo.username,
  avatar: state => state.user.userInfo.staffPhoto
}
export default getters
