import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import app from './modules/app'
import settings from './modules/settings'
import menu from './modules/menu'
import user from './modules/user'
Vue.use(Vuex)

// vuex 不等于 store

// 1. 区分vuex和 Vuex . Store实例化出来的store 不一样 我们用的是后者
// 2. 默认导出的写法 在导入时名字随便取 语义化即可
// 3．按需导出的时候 在导入的时候必须加上{}且名字必须对应

// 控制台出现这种错误
// [vuex] action type 'user/fetchLogin'
// 1．拼写错误
// 2．模块化 命名空间 namepaced:true
// 3. modules没有注册

const store = new Vuex.Store({
  // 组合模块的配置项
  // 模块化开发：
  // 1. 拆分出去的模块要在modules位置注册好 否则是不生效的
  // 2. 一旦开启了命名空间namespaced 调用mutation、action
  // this.$store.commit('模块名/mutation名字')
  // this.$store.dispatch('模块名/action名字')
  modules: {
    app,
    settings,
    menu,
    user
  },
  getters
})

export default store
