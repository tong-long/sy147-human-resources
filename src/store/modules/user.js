import { login, getUserInfo, getUserDetailById } from '@/api/user'
import { getToken, setToken, removeToken } from '@/utils/auth'
export default {
  namespaced: true,
  state: {
    // 短路运算符 前面有 true 用前面的 , 为假 用后面的
    token: getToken() || '', // 这里因为是初始值为空 刷新的时候所有的代码都会重置 token会被重置为''
    // 存储用户信息
    userInfo: {}
  },
  mutations: {
    setToken(state, tokenValue) {
      // 存入vuex
      state.token = tokenValue
      // 存入本地 cookie
      setToken(tokenValue)
      // 登录 -> token -> 存入cookie
      // 刷新 -> token初始化会执行  -> getToken
    },
    // 存储用户信息
    setUserInfo(state, userInfo) {
      state.userInfo = userInfo
    },
    // 点击退出登录清空数据 还有 tokrn 失效 调用这个方法
    remoUserInfo(state) {
      // 清空 vuex 中的数据
      state.token = ''
      state.userInfo = {}
      // 清空本地 token
      removeToken()
    }
  },
  actions: {
    async fetchLogin({ commit }, form) {
      // ctx.commit 提交mutation
      // ctx.dispatch 提交action
      // 1. 调用接口请求 登录
      const res = await login(form)
      // 2. 拿到请求返回值 提交mutation函数进行修改
      commit('setToken', res)
    },
    async fetchGetUserInfo({ commit }) {
      // 问题:
      // 1. 后端多个接口对应前端的一个业务 组合数据
      // 2. 接口之间有前后的依赖关系 await可以保证串行( 依次排队 )的结构
      // 1. 发请求
      const res = await getUserInfo()
      // 插入一个头像信息请求
      const res_ = await getUserDetailById(res.userId)
      // 2. 调用mutation [合并一下俩个接口的返回对象 成为一个对象]
      commit('setUserInfo', { ...res, ...res_ })
    }
  }
}

// 使用vuex管理数据的时候 从数据的定义到同步到异步都放到vuex中
// 组件只做一件事情 调用action传递参数
