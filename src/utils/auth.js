// 专门用来操作cookie的方法包
// 内部封装了繁琐的操作方法 参数处理 暴露三个函数 get,set,remove
// cookies原生写法非常麻烦 -> js-cookie -> 简化cookie的操作
// 在当前做持久化的场景下 cookie和ls没有区别 也是持久化的
import Cookies from 'js-cookie'
/**
 {
  get(){},  获取cookies中存入的token
  set(){},  存储方法往cookies存入token
  remove()  删除方法把cookies中的token删掉
 }
 */
const TokenKey = 'hr_token'

// 获取token的方法
export function getToken() {
  return Cookies.get(TokenKey)
}

// 设置方法
export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

// 删除方法
export function removeToken() {
  return Cookies.remove(TokenKey)
}
