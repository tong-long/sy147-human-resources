import router from '@/router'
import store from '@/store'
import axios from 'axios'
import { Message } from 'element-ui'
import { getToken } from './auth'
// 1. 实例化 create方法
// 定制axios的基地址和超时时间
// 基地址? 项目里面大部分的接口前面的协议域名端口都是一致的 只有后面的相对路径不一样
// 超时时间？ 接口从发送到返回容许最大时间差 100ms

// axios  vs service
// service = axios + 自定义的配置 （plus）
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API,
  timeout: 5000 // request timeout
})

// 请求拦截器
// 做什么呢？在正式接口发送之前 容许我们对接口参数做一些配置
// 具体的事情: 注入token 在请求头 -> 为了大部分需要鉴权的接口能够统一获取到用户标识
service.interceptors.request.use(
  config => {
    // 这里的return别丢！！！
    // 拿到token 存入请求头
    const token = getToken()
    if (token) {
      // 前面的authorization和后面的拼接方式都是固定的 业内规范
      config.headers['Authorization'] = `Bearer ${token}`
    }
    return config
  },
  error => {
    return Promise.reject(error)
  }
)

// 响应拦截器
// 做的什么事儿？ 在数据从后端返回到客户端的时候先被拦截下来 做一些自己的事儿
// 具体的事情: 1. 401 token失效 强制跳回到登录   2.简化组件中请求数据的访问
// 注意事项: 以http状态码决定的 2xx就执行第一个成功回调 不在200 - 300走第二个失败回调
// 谁决定当前的http状态 -> 后端

// 为什么选择这里解决？ 很多的接口都有此类问题 统一解决
// 问题: 后端不以200-300作为接口成功失败的标识 以一个自定义的success作为标识
// 前后端的依据不统一 永远走的都是成功回调
// 解决思路: 前端自己判断当前接口成功还是失败 如果成功还是按照之前的逻辑return 数据
// 如果失败 手动抛出错误 return Promise.reject()

service.interceptors.response.use(
  response => {
    // todo
    const { data, message, success } = response.data
    if (success) {
      // 接口成功
      return data
    } else {
      // 用一个警告框弹一下这个错误
      Message.warning(message)
      return Promise.reject(message)
    }
  },
  error => {
    // token 失效 或者 假的(假的就失效了) 服务端会判断 tokn 失效还是假的 会返回一个 401 错误
    console.dir(error) //  打印报错 response 里面有个 status 就是 报错的 状态码 401
    if (error.response.status === 401) {
      // 调用 方法 清空 tokrn 用户信息
      store.commit('user/remoUserInfo')
      // 跳转到登录页
      router.push('/login')
    }
    return Promise.reject(error)
  }
)

export default service

// axios封装好的实例对象以默认导出的方式 导出 -> api模块进行使用
