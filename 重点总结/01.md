# 现成的架子
1. 作用: 沉淀了很多通用的后台管理解决方案 提高开发效率
2. vue-admin-tempalte 很久不维护了


# 关键文件
1. 全局getters[基于state的计算属性]
解决问题：简化模块化开发模式下某些模块中的数据访问 长变短

this.$store.state.user.userInfo.username
this.$store.getters['username']

2. permission.js

1. 语义化维护 [所有和权限相关的操作]
2. 模块化中如果有同步代码 导入的时候会立刻执行


# request + api

1. request基础封装 axios
   1. 实例化 axios.create方法 baseURL + 超时时间
   2. 请求拦截器 注入token request.headers
   3. 响应拦截器 401 token失效  简化数据访问 response.data
      重点：成功和失败的回调执行依据 http code 是否200-300

2. api业务请求函数封装
   ```js
   function login (data){
     return request({
      url:'xxx',
      method:'POST',
      data
     })
   }
   login({参数}).then(res=>console.log(res);)
   ```

3. 一对多
   token统一生效   baseURL统一配置   401统一监控


# 环境变量

1. 什么是环境变量？
   可以随着环境的变化自动读取对应的环境变量 从而识别其中的不同的值

2. 如何定义使用？
   定义：key = value
   使用：process.env.key

3. 客户端 src下面使用时 命名需要遵守下面的命名规则
   VUE_APP_XXX


# 表单校验
1. 单独校验
   el-form  绑定表单对象 + 规则对象
   el-form-item   prop指定当前表单要使用哪条规则
   el-input  双向绑定

2. 兜底校验 
   获取一个组件实例对象 validate

3. 校验手段
   1. 内置规则 [简单的校验逻辑]
   2. 自定义校验函数 [更加的灵活 应付比较复杂的场景]
      [这个函数不一定要写到data()里面 可以写在script 可以写到某个js模块下 -> 作用域]


# 登录接口调用
1. 如何去看接口文档
   1. 请求路径
   2. 请求方法method
   3. 当前的请求参数  
       1. 查询字符串参数 params  url?id=1001
       2. 请求体参数  data  { mobile,password }
       3. path  url/:id   `url${id}`
   4. 请求实例 返回实例

2. 调试接口请求
   1. 打开network
   2. 打开payload
   3. 打开privew看返回